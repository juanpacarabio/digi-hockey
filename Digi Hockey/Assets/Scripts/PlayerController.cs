using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public bool isPlayer1; // Determina si es el jugador 1 o 2
    private Camera mainCamera;

    private void Start()
    {
        mainCamera = Camera.main;
    }

    private void Update()
    {
        foreach (Touch touch in Input.touches)
        {
            // Verificar si el toque est� en el lado del jugador correspondiente
            if ((isPlayer1 && touch.position.x < Screen.width / 2) ||
                (!isPlayer1 && touch.position.x >= Screen.width / 2))
            {
                // Convertir la posici�n del toque a coordenadas del mundo
                Vector3 touchPosition = mainCamera.ScreenToWorldPoint(new Vector3(touch.position.x, touch.position.y, 10f));

                // Mantener el eje Z constante (para 2D)
                touchPosition.z = 0f;

                // Mover el jugador directamente a la posici�n del toque
                transform.position = touchPosition;
            }
        }
    }
}